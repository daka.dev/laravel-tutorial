<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RequestDashboardTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_role_admin_success(): void
    {
        $response = $this->get('/admin/dashboard?role=admin');
        $response->assertStatus(200);
        $response->assertSeeText('This is Dashboard with role Admin');
    }

    public function test_role_pelatih_success(): void
    {
        $response = $this->get('/pelatih/dashboard?role=pelatih');
        $response->assertStatus(200);
        $response->assertSeeText('This is Dashboard with role Pelatih');
    }

    public function test_role_admin_unauthorized(): void
    {
        $response = $this->get('/admin/dashboard');
        $response->assertStatus(403);
    }

    public function test_role_pelatih_unauthorized(): void
    {
        $response = $this->get('/pelatih/dashboard');
        $response->assertStatus(403);
    }

    public function test_dashboard_role_admin_success(): void
    {
        $response = $this->get('/dashboard?role=admin');
        $response->assertStatus(200);
        $response->assertSeeText('This is Dashboard with role Admin');
    }

    public function test_dashboard_role_pelatih_success(): void
    {
        $response = $this->get('/dashboard?role=pelatih');
        $response->assertStatus(200);
        $response->assertSeeText('This is Dashboard with role Pelatih');
    }

    public function test_dashboard_role_unauthorized(): void
    {
        $response = $this->get('/dashboard');
        $response->assertStatus(403);
    }
}
