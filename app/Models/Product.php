<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Product extends Model
{
    use HasFactory;

    public function satuan(): BelongsTo
    {
        return $this->belongsTo(Satuan::class);
    }

    public function formatCurrency($value): string
    {
        return sprintf("Rp. %s", number_format($value, 0, ',', '.'));
    }
}
