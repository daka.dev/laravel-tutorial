<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Cart extends Component
{
    public $carts = [];

    protected $listeners = ['fromProductComponent' => 'actionMethod'];

    public function actionMethod($product)
    {
        $id = $product['id'];
        $price = $product['price'];

        $index = array_search($id, array_column($this->carts, 'id'));
        if ($index !== false) {
            $this->carts[$index]['qty']++;
            $this->carts[$index]['total'] += $price;
            return;
        }

        $data = [
            'id' => $id,
            'qty' => 1,
            'name' => $product['name'],
            'price' => $price,
            'total' => $price,
        ];

        $this->carts[] = $data;
    }

    public function formatCurrency($value): string
    {
        return sprintf("Rp. %s", number_format($value, 0, ',', '.'));
    }

    public function render()
    {
        return view('livewire.cart');
    }
}
