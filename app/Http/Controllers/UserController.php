<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->input('role') === 'admin') {
            return view('admin.dashboard');
        }

        return view('pelatih.dashboard');
    }
}
