<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Posts</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
        />
    </head>
    <body>
        <div class="card">
            @foreach ($posts as $post)
            <div class="card-body text-center">
                <button
                    type="button"
                    class="edit btn btn-primary"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    data-id="{{ $post->id }}"
                    data-name="{{ $post->name }}"
                    data-image="{{ $post->image }}"
                >
                    {{ $post->name }}
                </button>
            </div>
            @endforeach
        </div>

        <!-- Modal -->
        <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="name"
            aria-hidden="true"
        >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="name"></h5>
                    </div>
                    <div class="modal-body">
                        <img id="image" />
                        <input type="file" id="newImage" />
                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-secondary"
                            data-bs-dismiss="modal"
                        >
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Save
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script
            src="https://code.jquery.com/jquery-3.7.1.slim.js"
            integrity="sha256-UgvvN8vBkgO0luPSUl2s8TIlOSYRoGFAX4jlCIm9Adc="
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>

        <script>
            $(document).ready(function () {
                $(".edit").click(handleButtonClick);
                $("#newImage").change(handleImageChange);
            });

            function handleButtonClick() {
                const id = $(this).data("id");
                const image = $(this).data("image");
                const name = $(this).data("name");
                const previousId = $("#exampleModal").data("id");
                console.log(id, previousId);

                $("#name").text(name);

                if (!previousId || previousId !== id) {
                    $("#image").attr("src", image).attr("alt", name);
                    $("#exampleModal").data("id", id);
                }

                if (previousId !== id) {
                    $("#newImage").val("");
                }
            }

            function handleImageChange() {
                if (!this.files.length) return;

                const image = this.files[0];
                const reader = new FileReader();

                reader.onload = function (e) {
                    $("#image")
                        .attr("width", "150")
                        .attr("height", "150")
                        .attr("src", e.target.result)
                        .attr("alt", image.name);
                };

                reader.readAsDataURL(image);
            }
        </script>
    </body>
</html>
