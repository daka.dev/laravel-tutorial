<div class="mt-3">
    @if(count($carts) > 0)
    <table class="table">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col" class="text-center">Qty</th>
                <th scope="col">Product</th>
                <th scope="col">Harga</th>
                <th scope="col" class="text-end">Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach($carts as $cart)
            <tr>
                <th scope="row">{{ $cart["id"] }}</th>
                <td class="text-center">{{ $cart["qty"] }}</td>
                <td>{{ $cart["name"] }}</td>
                <td>{{ $this->formatCurrency($cart["price"]) }}</td>
                <td class="text-end">
                    {{ $this->formatCurrency($cart["total"]) }}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <h5 class="text-dark text-center text-decoration-underline">
        Data Cart Not Found
    </h5>
    @endif
</div>
