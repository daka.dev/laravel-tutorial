<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Products</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
        />
        @livewireStyles
    </head>
    <body>
        <div class="container">
            <div class="d-flex justify-content-center mt-3">
                <div class="row">
                    @foreach($products as $product)
                    <div class="col">
                        <div class="card" style="width: 18rem">
                            <div class="card-body">
                                <h5 class="card-title">{{ $product->name }}</h5>
                                <hr />
                                <p class="card-text">
                                    {{ $product->qty }}
                                    {{ $product->satuan->name }} &rarr;
                                    {{ $product->formatCurrency($product->price) }}
                                </p>

                                <button
                                    class="btn btn-primary"
                                    type="button"
                                    class="btn btn-primary"
                                    data-bs-toggle="modal"
                                    data-bs-target="#exampleModal"
                                    onclick="setSatuanId({{ $product->satuan->id }});"
                                >
                                    Edit
                                </button>

                                @livewire('product', compact('product'))
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <!-- Carts -->
            <div class="row">
                <div class="col-8 mx-auto">@livewire('cart')</div>
            </div>
        </div>

        <!-- Modal -->
        <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
        >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            Edit Satuan
                        </h5>
                        <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                        ></button>
                    </div>
                    <div class="modal-body">
                        <select class="form-select" id="select">
                            @foreach($satuans as $satuan)
                            <option value="{{ $satuan->id }}">
                                {{ $satuan->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-secondary"
                            data-bs-dismiss="modal"
                        >
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Update Product
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
            crossorigin="anonymous"
        ></script>
        <script>
            function setSatuanId(satuanId) {
                let selectElement = document.querySelector("#select");

                for (let option of selectElement.options) {
                    if (option.value == satuanId) {
                        option.selected = true;
                        break;
                    }
                }
            }
        </script>
        @livewireScripts
    </body>
</html>
