<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Datatables</title>

        <!-- CSS Datatables -->
        <link
            rel="stylesheet"
            type="text/css"
            href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css"
        />
    </head>
    <body>
        <h1>Example Data (Server => Pagination Mount From Server)</h1>
        <table id="myTable" class="display">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Image</th>
                </tr>
            </thead>
            <tbody>
                <!-- Data akan dimuat di sini melalui AJAX -->
            </tbody>
        </table>
    </body>

    <script
        src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo="
        crossorigin="anonymous"
    ></script>
    <script
        type="text/javascript"
        src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.js"
    ></script>
    <script>
        $(document).ready(function () {
            // server-side
            $("#myTable").DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('posts.server') }}",
                    type: "GET",
                },
                columns: [
                    { data: "id", name: "id" },
                    { data: "name", name: "name" },
                    {
                        data: "image",
                        orderable: false,
                        searchable: false,
                    },
                ],
            });
        });
    </script>
</html>
