<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Posts</title>
        <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
            crossorigin="anonymous"
        />
    </head>
    <body>
        <div class="card">
            @foreach ($posts as $post)
            <div class="card-body text-center">
                <button
                    type="button"
                    class="edit btn btn-primary"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                    data-id="{{ $post->id }}"
                    data-title="{{ $post->title }}"
                    data-slug="{{ $post->slug }}"
                    data-category="{{ $post->category->name }}"
                    data-content="{{ $post->content }}"
                >
                    {{ $post->title }}
                </button>
            </div>
            @endforeach
        </div>

        <!-- Modal -->
        <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="name"
            aria-hidden="true"
        >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Post</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-4">
                                <div class="input-group mb-3">
                                    <span>Category Name</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="input-group mb-3">
                                    <input
                                        id="category"
                                        type="text"
                                        class="form-control"
                                        readonly
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <div class="input-group">
                                    <span>Slug</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="input-group">
                                    <input
                                        id="slug"
                                        type="text"
                                        class="form-control"
                                        readonly
                                    />
                                </div>
                            </div>
                        </div>
                        <hr class="border border-2 border-dark" />
                        <div class="row">
                            <div class="col-4">
                                <div class="input-group mb-3">
                                    <span>Title</span>
                                </div>
                            </div>
                            <div class="col">
                                <div class="input-group mb-3">
                                    <input
                                        id="title"
                                        type="text"
                                        class="form-control"
                                        placeholder="title"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-4">
                                <span>Content</span>
                            </div>
                            <div class="col">
                                <div class="input-group">
                                    <textarea
                                        class="form-control"
                                        id="content"
                                        rows="5"
                                    ></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button
                            type="button"
                            class="btn btn-secondary"
                            data-bs-dismiss="modal"
                        >
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script
            src="https://code.jquery.com/jquery-3.7.1.slim.js"
            integrity="sha256-UgvvN8vBkgO0luPSUl2s8TIlOSYRoGFAX4jlCIm9Adc="
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
            integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
            crossorigin="anonymous"
        ></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
            integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
            crossorigin="anonymous"
        ></script>

        <script>
            $(document).ready(function () {
                $(".edit").click(handleButtonClick);
            });

            function handleButtonClick() {
                const id = $(this).data("id");
                const title = $(this).data("title");
                const slug = $(this).data("slug");
                const category = $(this).data("category");
                const content = $(this).data("content");

                $("#title").val(title);
                $("#slug").val(slug);
                $("#category").val(category);
                $("#content").val(content);
            }
        </script>
    </body>
</html>
