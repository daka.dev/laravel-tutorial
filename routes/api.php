<?php

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('posts/client', function () {
    $datas = [
        (object)
        [
            'id' => 1,
            'name' => 'Pertama',
            'image' => 'https://via.placeholder.com/150',
        ],
        (object)
        [
            'id' => 2,
            'name' => 'Kedua',
            'image' => 'https://via.placeholder.com/150',
        ]
    ];

    return response([
        'message' => 'success',
        'data' => collect($datas)->all()
    ]);
})->name('posts.client');

// example manual
// recommended with package Yajra\DataTables
Route::get('posts/server', function (Request $request) {
    $datas = [
        (object) ['id' => 1, 'name' => 'Pertama', 'image' => 'https://via.placeholder.com/150'],
        (object) ['id' => 2, 'name' => 'Kedua', 'image' => 'https://via.placeholder.com/150'],
    ];

    $totalData = count($datas);

    // Searching
    $searchValue = $request->input('search.value');
    $searchableColumns = ['id', 'name'];

    if ($searchValue) {
        $datas = array_filter($datas, function ($item) use ($searchValue, $searchableColumns) {
            foreach ($searchableColumns as $column) {
                if (stripos($item->$column, $searchValue) !== false) {
                    return true;
                }
            }
            return false;
        });
    }

    $totalFiltered = count($datas);

    // Sorting
    $orderColumn = $request->input('order.0.column');
    $orderDir = $request->input('order.0.dir');
    $columns = ['id', 'name', 'image'];

    if ($orderColumn !== null && $orderDir) {
        $datas = collect($datas)->sortBy($columns[$orderColumn], SORT_REGULAR, $orderDir == 'desc')->values()->all();
    }

    // Pagination
    $start = intval($request->input('start'));
    $length = intval($request->input('length') > 0 ? $request->input('length') : 10);
    $page = ($start / $length) + 1;

    $slicedData = collect($datas)->forPage($page, $length)->values()->all();
    $paginated = new LengthAwarePaginator($slicedData, $totalFiltered, $length, $page, [
        'path'  => $request->url(),
        'query' => $request->query(),
    ]);

    return response([
        'draw'            => intval($request->input('draw')),
        'recordsTotal'    => $totalData,
        'recordsFiltered' => $totalFiltered,
        'data'            => $paginated->items(),
    ]);
})->name('posts.server');
