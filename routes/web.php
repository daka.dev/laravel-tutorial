<?php

use App\Http\Controllers\UserController;
use App\Models\Post;
use App\Models\Product;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// endpoint:port/admin/dashboard?role=admin
Route::group(['middleware' => ['role:admin'], 'prefix' => 'admin'], function () {
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard.admin');
});

// endpoint:port/pelatih/dashboard?role=pelatih
Route::group(['middleware' => ['role:pelatih'], 'prefix' => 'pelatih'], function () {
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard.pelatih');
});

// endpoint:port/dashboard?role=admin
// endpoint:port/dashboard?role=pelatih
Route::middleware('roleDynamic:admin,pelatih')->group(function () {
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard');
});

// example main page
Route::get('main', function () {
    $datas = [
        (object)[
            'id' => 1,
            'name' => 'Pertama',
        ],
        (object)[
            'id' => 2,
            'name' => 'Kedua',
        ]
    ];

    return view('example', compact('datas'));
});

// example show single data with modal using ajax
Route::get('main/{id}', function ($id) {
    $datas = [
        [
            'id' => 1,
            'name' => 'Pertama',
        ],
        [
            'id' => 2,
            'name' => 'Kedua',
        ]
    ];

    $data = collect($datas)->where('id', $id)->first();
    if (!$data) {
        return response(['message' => 'Data not found'], 404);
    }

    return response(['data' => $data]);
});

Route::get('posts', function () {
    $datas = [
        (object)
        [
            'id' => 1,
            'name' => 'Pertama',
            'image' => 'https://via.placeholder.com/150',
        ],
        (object)
        [
            'id' => 2,
            'name' => 'Kedua',
            'image' => 'https://via.placeholder.com/150',
        ]
    ];

    $posts = collect($datas)->all();
    return view('posts.index', compact('posts'));
});

// example data with db
Route::get('blogs', function () {
    $posts = Post::with('category')->get();
    return view('blogs.index', compact('posts'));
});

// example datatables
Route::get('datatables/client', function () {
    return view('datatables.client');
});

Route::get('datatables/server', function () {
    return view('datatables.server');
});

Route::get('products', function () {
    $products = Product::with('satuan')->get();
    $satuans = Satuan::all();
    return view('products.index', compact('products', 'satuans'));
});


Route::middleware('guest')->group(function () {
    Route::get('login', function () {
        return view('auth.login');
    })->name('login');

    // Login wrong with OLD value
    Route::post('login', function (Request $req) {
        // validation input
        $data = $req->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // check users in database
        if (Auth::attempt($data)) {
            $req->session()->regenerate();

            return to_route('home');
        }

        return back()->withInput()->with('message', 'Email or Password incorrect');
    })->name('login.store');
});

Route::middleware('auth')->group(function () {
    Route::get('home', function () {
        return view('home');
    })->name('home');

    Route::delete('logout', function (Request $req) {
        Auth::logout();

        $req->session()->invalidate();
        $req->session()->regenerateToken();

        return redirect('/');
    })->name('logout');
});
